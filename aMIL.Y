
// aMIL.Y software ver 0.2 
// for two stepping motors: M1 is Z, M2 is Y
// with externally given var positions
// by Dmitry Zyabkin 
// dmitry.zyabkin@cern.ch
// 14.12.2022
#include <Stepper.h>
const int stepsPerRevolution1 = 400; // the number of steps per full motor revolution (Z motor does 1600 for a full circle)
const int stepsPerRevolution2 = 800; // the number of steps per full motor revolution (Y motor does 800 for a full circle) with Velocity + goes to +
// M00,VEL,S,STEEEPS, S for sign
String readString, Motor1, Motor2, NN, VEL, S, STEEEPS;
//
Stepper myStepper1(stepsPerRevolution1, 2, 3);
Stepper myStepper2(stepsPerRevolution2, 9, 8);
// Stepper myStepper1(stepsPerRevolution, 3, 2); otherwise with + Speed it moves negatively
// Stepper myStepper2(stepsPerRevolution, 8, 9);
int stepCount = 0; // number of steps the motor has taken
String InBytes;
String SerialBuffer;
void setup() {
  Serial.begin(115200);
  Serial.flush();
  pinMode(LED_BUILTIN, OUTPUT);
  myStepper1.setSpeed(150); // Stepper 1 is Z-Axis
  myStepper2.setSpeed(150); // Stepper 2 is Y-Axis
}
void loop() {
  // test_Response(); // Serial Test
  Movements();
}
//------ LED test, if PySerial responses available--------
void Test_Response() {
if (Serial.available()>0){
  InBytes = Serial.readStringUntil('\n');
  if (InBytes == "on") {
    digitalWrite(LED_BUILTIN,HIGH);
    Serial.println("LED ON");
    // Serial.write("LED ON"); this waits for the timeout
  }
  if (InBytes == "off") {
    digitalWrite(LED_BUILTIN,LOW);
    // Serial.write("LED OFF");
    Serial.println("LED OFF");
  }
  else {
    Serial.println("Invalid Input");
    // Serial.write("Invalid Input");
  }
  }
}
//------ PySerial obtaining the String, getting Motor, Vel and Steps from it-----
void Movements() {
if (Serial.available()>0) {
  SerialBuffer = Serial.readStringUntil('\n'); // copying the message
  Serial.println("Received command: " + SerialBuffer);
  NN = SerialBuffer.substring(0, 1); //get the first character
  VEL = SerialBuffer.substring(1, 4); //get the VELOCITY characters
  // S = SerialBuffer.substring(4, 5); //Movement sign upd. Obsolete. S included into Steps. dz
  STEEEPS = SerialBuffer.substring(4, 14); //get the first four characters
  Serial.println(" Nn: " + NN + "Steps: " + STEEEPS);
  int n1 = NN.toInt();
  int n2 = VEL.toInt();
  // int n3 = S.toInt(); obsolete. dz
  int n4 = STEEEPS.toInt();
  if (SerialBuffer.startsWith("1")) {
    Serial.println("Selected Motor 1 ");
    digitalWrite(LED_BUILTIN,HIGH);
    myStepper1.step(n4);
    if (n2 != 150){
      myStepper1.setSpeed(n2);} // Stepper 1 is Z-Axis
  }
  if (SerialBuffer.startsWith("2")) {
    Serial.println("Selected Motor 2 ");
    digitalWrite(LED_BUILTIN,HIGH);
    myStepper2.step(n4);
    if (n2 != 150){
      myStepper2.setSpeed(n2);} // Stepper 2 is Z-Axis
  }
  else {
    Serial.println("Invalid Input");
    Serial.println("LED OFF");
  }
  }
  }
